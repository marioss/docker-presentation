
        cd php-fpm
        docker build --tag mions/jenmi_phpfpm:1.0.0 .

        cd ../nginx
        docker build --tag mions/jenmi_nginx:1.0.0 .

        docker run -d \
        --name=test_php \
        --volume=/Users/mssio/Codes/work/019-jenmi/001-docker/source/practice/002-webapp-without-compose/volume:/usr/share/nginx/html \
        mions/jenmi_phpfpm:1.0.0

        docker run -d \
        --name=test_nginx \
        --volume=/Users/mssio/Codes/work/019-jenmi/001-docker/source/practice/002-webapp-without-compose/volume:/usr/share/nginx/html \
        --link=test_php:webapp-fpm \
        -p 80:80 \
        mions/jenmi_nginx:1.0.0

1. Explain a little about docker-machine, docker client, docker host thing.
2. Explain about image tag on build must be same with registry tag.
3. Explain about name, volume mapping, port mapping
4. Explain about docker ps, how to stop and remove container
