
### Introduction to Docker image

> Like I have explained before, docker image is read-only template with instructions for creating a Docker container. To understand a little bit more about this let's take a look at this example of hello world image

Open https://github.com/docker/dockercloud-hello-world

> We have a few files here, but the file that will generate the docker images is this file called a Dockerfile. Let's take a look inside this file.

> The FROM command is the base image. Usually we will use a lightweight Linux distribution image like alpine for the base image. It can also be ubuntu, fedora or any other distributions if you like, but it will be to heavy / large if you don't need most of the feature from that distribution.

> So on this example the image is from Alpine Linux distribution. Alpine is a really small linux, it is also great Linux distribution because it has large selection of packages from the repository like Ubuntu apt repository or Red Hat (CentOS) yum repository.

> Next line, this command will run installation of nginx & php-fpm inside this image. It will also setup log file for nginx & socket file to run this nginx.

> Next line, it will add www directory from this (back from github folder and open www dir) to the image /www path

> Next line, it will override default nginx.conf to this nginx.conf (also back and open nginx.conf)

> Next line, same as before but this time php-fpm.conf

> Next line, there is an expose command. This command will informs Docker that the container listens on the specified network ports at runtime.

> Next line, it will run 3 commands simultaneously, which is run php-fpm server, tail nginx access log, and run nginx server.

> Now that we have understand how Dockerfile works, let's take a look at how the container will be run from this image. First thing before running any image we should pull the image to our dokcer host. If we do not pull it first, docker host can automatically search for it on default registry, but to make sure everything run smoothly (no errors because of internet connection problem or anything) I prefer to pull the image before running any container.

        docker pull dockercloud/hello-world
        docker run --rm -p 80:80 dockercloud/hello-world

> --rm is cleanup arguments. It will automatically destroy the container when the process is stopped.

> -p is ports configuration. It will map port from container to the docker host, so it means now docker host is running this hello-world app on port 80. Let's try it.
