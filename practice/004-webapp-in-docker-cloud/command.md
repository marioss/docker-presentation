
        docker login

Step by step:

1. Docker login
2. Push images
3. Link Digital Ocean to Docker Cloud
4. Create node cluster
5. Create stack
6. Run volume container
7. Copy source code to volume container
8. Run the rest of the container
9. Cleanup
