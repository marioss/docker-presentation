# Presentation Speech (ID)

### Slide 1 (Title Page)

> Selamat sore semuanya,
> Hari ini saya akan presentasi tentang Docker. Saya akan menjelaskan bagaimana Docker dapat membantu kita untuk develop dan deploy web application dengan lebih mudah.

> {Prologue}

> This presentation is based on video presentation by Solomon Hykes on Youtube with title: dotScale 2013 - Solomon Hykes - Why we built Docker. I have copied a lot of materials from that video & add few things for this presentation. You may see that video if you don't understand with this presentation because right now I can't explain as good as him on that video.
> Solomon Hykes is currently the founder, CTO, and Chief Products Officer of the Docker Inc. (Based on information in January 3rd 2017)

### Slide 2 (Outline)

> Ini adalah outline dari presentasi saya.

### Slide 3 (The What Question)

> Apa itu Docker? Docker adalah portable container engine. Untuk mengetahui lebih lanjut, kita akan membahas masalah yang dipecahkan oleh Docker.

### Slide 4

> Ini adalah tentang shipping software. Ketika kalian menjalankan code di Machine A dan Machine B, kalian berusaha agar program memiliki behavior yang sama di Machine A dan Machine B. Masalahnya adalah, kita ingin proses in dapat diandalkan & automated, tetapi seringnya hal ini tidak dilakukan.

### Slide 5

> Sebagai contoh tentang maksud dari shipping adalah ketika kalian share development environment dari komputer kalian ke komputer teman.

### Slide 6

> Berikutnya adalah ketika kalian deploy code ke server staging / production. Kalian melakukan shipping dari local server ke production server.

### Slide 7

> Sama juga dengan ketika kalian scal aplikasi dari satu server ke banyak server, jadi kalian shipping code ke banyak server yang berbeda.

### Slide 8

> Dan yang terakhir, jika kalian migrate dari hosting provider ke provider lain atau mungkin ke datacenter perusahaan sendiri, kalian akan shipping code ke lebih banyak server lagi.

> Kalian akan berharap code tersebut memiliki behavior yang sama, reliable dan atuomated. Hal ini akan sangat sulit untuk dilakukan apabila servernya sudah sebanyak yg di gambar ini.

> Karena semakin aplikasi kita berkembang, software stack kita akan menjadi semakin kompleks seperti ini. (next slide)

### Slide 9

> So you have got a really complex software stack and it is running on increasingly complex hardware infrastructure. I don't think that I should explain that software stack one by one because even I don't understand about some of it, but it is pretty complex and it is dependent on a lot of specific language version and also third party library. But once you have created this application you will have a responsibility to make sure that it will run on any machine that want to run it and behave the same way everywhere.

> Then you will have this matrix from hell to make sure that everything works everywhere.

### Slide 10

> Every software component in your stack multiplied by every element of your infrastructure that it will be run on. Every intersection of this matrix has to work. Each element of the infrastructure may have different supported software version, different operating system, and a lot of other different things that may break your application.

### Slide 11

> From that matrix, it can be said that shipping software from A to B is a brittle and very labor-intensive process. So how do we fix this situation.

> The first thing we can do is look for example of other people who have this problem before and fix it. And we have an example on the shipping industry.

### Slide 12

> So the shipping industry obviously is in the business of moving physical things across the world from point A to point B and has been run for centuries. It also has been done on similar way on how should we ship software.

### Slide 13

> On shipping industry we have a lot of different shape of goods like crate box, barrel, car, piano, etc. And we also have a lot of different way to ship it like using car, train, ship, plane, forklift, etc.

> We will also care about how the items is being shipped. For example if we want to ship coffee beans, we will want to know what is shipped together with our coffee beans. Will it be shipped together with piano and and the piano will be sitting on the coffee beans so it will be crushed when arrived at destination. The staff at Rotterdam need know about how to handle that.

> So on the shipping industries we have different type of goods that need to be shipped using different kind of infrastructures.

> So here it comes, another matrix from hell for shipping industry.

### Slide 14

> It will be also need a very labor-intensive process to handle it like the one on how should we ship software application. This problem has been there for a very long time until one day in 1950s a few people got together on the shipping industry and agreed on the standard box that, they agreed on the dimension, agreed on the weight, agreed on the how the doors will open. So they agreed on format and they agreed on standard operation, basically they agreed on API and started using it.

### Slide 15

> And so shipping container was born. This box change the world of shipping industry. It enable the separation of concern. For example if I want to ship coffee, I just put it to a container with any other stuff that I want too. I closed the door put a tag to identify the container then ship it. And from that point on, getting the container to the other side of the world is no longer my problem. The infrastructure provider will take care of that and all I have to do is just wait for the container to show up on the other side. I break the seal, I open the door. The infrastructure provider also does not need to care about what is the content of the box. All they have to do is just ship it. That same container can also be put on all different infrastructure providers.

> This separation of concern is very powerful, because with this will come automation and then with automation also come reliability.

### Slide 16

> Why it has not been done before? Maybe you guys have some ideas that this kind of technology is nothing new and it has existed before Docker is created. For example:

### Slide 17

> You may think that I write Java and I used jars, isn't that a container? Or maybe I write Python and use virtualenv, isn't that a container too? Yeah right both of these tools can sandbox our code. They enable us to put something in container, but we can't put everything in that container. If our code depends on system library or any other library from another language, those tools can't help us to make this sandboxing happen. So we can said that his sandboxing method provided by those tools are INCOMPLETE.

> Okay then next options is to use vm (virtual machines). If Python packages aren't enough, jars aren't enough, let's just take the whole machine, let's put the software in it. Just share the computer along with it, that way we are guaranteed to have the same context for everyone. That is actually a very good idea. The problem though with virtual machine is that they bundle too much. You do want the whole system, but you do not want as a developer to package things like the whole virtual set of processors and network interfaces. You don't want to be deciding as a developer how storage is going to work for this application, how networking is going to work, how much RAM there's going to be. You can't do that because that will break separation of concerns. The infrastructure provider is not free about how to implement your application. In the metaphor of shipping containers, the shipping company should be free to choose the crane, should be free to choose the boat. In fact that I am giving them coffee bags to ship doesn't mean I can tell them which crane to use. That's the whole point.

> Another problem for virtual machines is that they are too heavy. If your whole stacks include 10 components to run 10 vms on the laptop will probably just make your laptop freezing. So I can say that the implementation using virtual machines is IMPRACTICAL to use if we have a bigger project.

> So what other options do we have?

### Slide 18

> This is where the set of discoveries that lead to Docker come to play. There is a way to get the best of both worlds. Best way to describe the best of both worlds is:

> I want to sandbox the entire system so as a developer, I am going to have a guarantee that what I am going to ship is going to be repeatable but I don't want to ship the machine details because that's too much and I don't want performance hit on a VMs.

### Slide 19

> So this is where Docker can solve these problems. Thanks to all Linux kernell hackers in the world that finally have implemanted namespacing networks. What that means is you can now use modern Linux Kernel, isolate any process from the others, and basically make that process believe that it has actually its own vm when really it doesn't.

> So the developers on company named Dot Cloud is working on these Linux kernel features to create Docker. It has the best of both worlds, it has complete sandboxing, fast, works with all languages. It is really really fast, it can run most container in less than 5 seconds.

### Slide 20

> What Docker did is 3 things:

> The first thing is, it define a standard container format to ship a software.

### Slide 21

> Then Docker create tools for developer to build a source code into a container, regardless of the languages, regardless of the build tools.

> The third thing is simple tools for ops team to take that container without having to know what's in it and then run it. Run it hopefully on as many machines as possible.

> So that's how Docker works in essence. The good thing about Docker is that it is compatible with current tools used by developer and ops team to build & ship a software. Developer does not need to forget about jars, makefile, etc to learn whole new packaging tool and the ops team also does not need to forget about chef, puppet, etc. Docker is creating the ingredients that is very lightweight to not get in the way. It's designed to improve your existing tools, and if you integrate it into your tools then all of sudden things will start getting really awesome.

### Slide 22

> This is how Docker architecture looks like.

> Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

> Docker host is the server who will run Docker container. It can be same as the client if we are working on our local computer, but it can also be set to run on different computer and controlled by another computer. Docker host need to pull images from the registry before running it or it can also run built images from the client. This Docker Host will be controlled using docker command.

> The Docker daemon runs on a host machine. The user uses the Docker client to interact with the daemon.

> The Docker client, in the form of the docker binary, is the primary user interface to Docker. It accepts commands and configuration flags from the user and communicates with a Docker daemon. One client can even communicate with multiple unrelated daemons.

> To understand Docker�s internals, you need to know about images, registries, and containers.

> A Docker image is a read-only template with instructions for creating a Docker container. For example, an image might contain an Ubuntu operating system with Apache web server and your web application installed. You can build or update images from scratch or download and use images created by others. An image may be based on, or may extend, one or more other images. A docker image is described in text file called a Dockerfile, which has a simple, well-defined syntax.

> Docker images are the build component of Docker.

> A Docker container is a runnable instance of a Docker image. You can run, start, stop, move, or delete a container using Docker API or CLI commands. When you run a container, you can provide configuration metadata such as networking information or environment variables. Each container is an isolated and secure application platform, but can be given access to resources running in a different host or container, as well as persistent storage or databases.

> Docker containers are the run component of Docker.

> A docker registry is a library of images. A registry can be public or private, and can be on the same server as the Docker daemon or Docker client, or on a totally separate server.

> Docker registries are the distribution component of Docker.

### Slide 23

> To understand a little more about Docker, I will give you a demo.

### Slide 24

> Thanks a lot for having me.
